package com.example.twininterview.android.ui.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Filter
import android.widget.Filterable
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.ContextCompat.getDrawable
import com.example.twininterview.android.R
import com.example.twininterview.android.adapter.itemdecoration.DividerItemDecoration
import com.example.twininterview.android.databinding.FragmentHomeBinding
import com.example.twininterview.android.ui.base.BaseFragment
import com.example.twininterview.android.ui.home.adapter.FoodAdapter
import com.example.twininterview.android.ui.utilities.hideKeyboard
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), Filter.FilterListener {
    override val layoutId: Int = R.layout.fragment_home
    override val viewModelClass = HomeViewModel::class
    private val foodAdapter by lazy {
        FoodAdapter()
    }

    private lateinit var mSearchView:SearchView

    override fun init(savedInstanceState: Bundle?) {
     viewModel.load()
     viewModel.errorMessage.observe(this){message->
         if(!message.isNullOrBlank()) {
             Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
             binding.foodList.visibility = View.GONE
         }
         }
      setHasOptionsMenu(true)

    }

    override fun initView(savedInstanceState: Bundle?) {
     binding.lifecycleOwner=this
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        val recyclerView=binding.foodList
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(DividerItemDecoration(getDrawable(requireContext(),R.drawable.divider)))
        recyclerView.adapter=foodAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.main_menu, menu)
        val searchItem = menu.findItem(R.id.action_search)
        mSearchView = searchItem.actionView as SearchView
        mSearchView.setIconifiedByDefault(true)
        mSearchView.imeOptions = EditorInfo.IME_ACTION_SEARCH
        mSearchView.isIconified = true
        mSearchView.isFocusable = true
        mSearchView.requestFocusFromTouch()
        mSearchView.queryHint ="Search"
        val editText = mSearchView.findViewById<EditText>(R.id.search_src_text)
        editText.setHintTextColor(getColor(requireContext(), R.color.black))
        editText.setTextColor(getColor(requireContext(), R.color.white))

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                mSearchView.hideKeyboard()
                return true
            }


            override fun onQueryTextChange(newText: String): Boolean {
                //do something
                val query=newText.trimStart().trimEnd()
                if(query.isNotEmpty())
                startFilter(query)
                else
                 foodAdapter.resetFoodItemList()
                return false
            }
        })

    }

    private fun startFilter(query:CharSequence) {
        (foodAdapter as Filterable).filter.filter(query,this)
    }

    override fun onFilterComplete(p0: Int) {}
}
