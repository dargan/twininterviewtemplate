package com.example.twininterview.android.data.domain.food.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.twininterview.android.BR
import com.google.gson.annotations.SerializedName

data class  Food (
    val id: String,
    val title: String,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("is_recent")
    val isRecent: Boolean,
    @SerializedName("is_tfy")
    val isTfy: Boolean,


):BaseObservable() {

    private var isSelected:Boolean=false

    @Bindable
    fun getIsSelected():Boolean {
        return isSelected
    }

    fun setIsSelected(isSelected:Boolean)
    {
        this.isSelected=isSelected
        notifyPropertyChanged(BR.isSelected)
    }

}