package com.example.twininterview.android.ui.home.usecase

import com.example.twininterview.android.data.domain.base.Usecase
import com.example.twininterview.android.data.domain.food.model.Food

interface HomeUseCaseInterface:Usecase<Unit,Unit> {
   suspend  fun getFoodList():Pair<List<Food>?,Throwable?>
}