package com.example.twininterview.android.ui.home

import androidx.lifecycle.MutableLiveData
import com.example.twininterview.android.data.domain.base.invoke
import com.example.twininterview.android.data.domain.food.model.Food
import com.example.twininterview.android.data.domain.food.repository.InMemoryFoodDataRepository
import com.example.twininterview.android.ui.base.BaseViewModel
import com.example.twininterview.android.ui.home.usecase.HomeUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class HomeViewModel @Inject constructor(

) : BaseViewModel() {
    @Inject
     lateinit var homeUseCase: HomeUseCase
     val foodListLiveData=MutableLiveData<MutableList<Food>>()
     val errorMessage = MutableLiveData<String>()


    fun load() {
     launch(Dispatchers.IO) {
         homeUseCase.getFoodList().first?.toMutableList().also { foodListLiveData.postValue(it)  }
         homeUseCase.getFoodList().second?.localizedMessage.also{errorMessage.postValue(it)}
     }

    }


}
