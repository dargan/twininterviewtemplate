package com.example.twininterview.android.ui.home.usecase

import com.example.twininterview.android.data.domain.food.model.Food
import com.example.twininterview.android.data.domain.food.repository.InMemoryFoodDataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.invoke
import javax.inject.Inject

@ExperimentalCoroutinesApi
class HomeUseCase @Inject constructor():HomeUseCaseInterface {
   @Inject
   lateinit var repository: InMemoryFoodDataRepository
    override suspend fun getFoodList():Pair<List<Food>?,Throwable?> =(Dispatchers.IO) {
        repository.getFood()
    }



    override suspend fun invoke(param: Unit) {}
}