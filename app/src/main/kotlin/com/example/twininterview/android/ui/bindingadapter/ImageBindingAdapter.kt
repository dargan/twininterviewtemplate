package com.example.twininterview.android.ui.bindingadapter

import android.content.Context
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.twininterview.android.R


@BindingAdapter("imageUrl")
fun loadImageURL(imageView: ImageView, imageUrl: String) {
    Glide.with(imageView.context)
        .load(imageUrl)
        .error(R.color.background_grey)
         .transition(DrawableTransitionOptions.withCrossFade())
        .placeholder(R.drawable.ic_launcher_round)
        .dontAnimate()
        .apply(RequestOptions.circleCropTransform())
        .into(imageView)

}


