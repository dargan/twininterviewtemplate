package com.example.twininterview.android.ui.home.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.twininterview.android.adapter.viewholder.DefaultHolder
import com.example.twininterview.android.data.domain.food.model.Food
import com.example.twininterview.android.databinding.ListItemFoodHomeBinding
import com.example.twininterview.android.ui.bindingadapter.RecyclerBindingContract
import java.util.*
import kotlin.collections.ArrayList


internal class FoodAdapter(

) : RecyclerView.Adapter<FoodAdapter.FoodItemViewHolder>(), RecyclerBindingContract<ArrayList<Food>>,Filterable {

    private var foodItems = arrayListOf<Food>()
    lateinit var sourceFoodList:ArrayList<Food>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return FoodItemViewHolder(ListItemFoodHomeBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: FoodItemViewHolder, position: Int) {
        holder.bind(foodItems[position])
    }

    override fun getItemCount(): Int {
        return foodItems.size
    }

    override fun setData(data: ArrayList<Food>) {
        foodItems=data
        sourceFoodList=data
        notifyDataSetChanged()
    }

    inner class FoodItemViewHolder(private val foodItemBinding: ListItemFoodHomeBinding):DefaultHolder<ListItemFoodHomeBinding>(foodItemBinding){
     fun bind(foodItem: Food) {
      foodItemBinding.setVariable(BR.foodItem, foodItem)
     }
    }

    override fun getFilter(): Filter {
        return object :Filter(){
            override fun performFiltering(searchSequence: CharSequence?): FilterResults {
            val filterRes=FilterResults()
                if (!TextUtils.isEmpty(searchSequence)) {

                    val searchData=ArrayList<Food>()
                    val locale=Locale.getDefault()
                         sourceFoodList.forEach { foodItem->
                         if (foodItem.title.toLowerCase(locale).startsWith(searchSequence.toString().toLowerCase(locale))) {
                             searchData.add(foodItem)
                         }
                     }
                    // Assign the data to the FilterResults
                    filterRes.values = searchData
                    filterRes.count = searchData.size
                }
                return filterRes

            }

            override fun publishResults(searchSequnce: CharSequence?, results: FilterResults?) {
                if (results?.values != null) {
                    foodItems = results.values as ArrayList<Food>
                    notifyDataSetChanged()
                }
            }

        }
    }
    fun resetFoodItemList()
    {
        foodItems=sourceFoodList
        notifyDataSetChanged()
    }

}
