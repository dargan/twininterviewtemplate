package com.example.twininterview.android.data.domain.food.repository

import android.util.Log
import com.example.twininterview.android.InterviewApplication
import com.example.twininterview.android.data.domain.food.model.Food
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InMemoryFoodDataRepository @Inject constructor(

) : FoodDataRepository {

    @Inject
    lateinit var context:InterviewApplication
    private val fileName="food.json"

    override suspend fun loadFood(): Pair<Boolean?, Throwable?> {
        return Pair(true, null)
    }

    override suspend fun getFood(): Pair<List<Food>?, Throwable?>  {
        return getDataFromJson()
    }

    private fun getDataFromJson(): Pair<List<Food>?, Throwable?> {
        val jsonString: String
        return try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
            Log.i("data", jsonString)
            val foodList = object : TypeToken<List<Food>>() {}.type
            Pair(Gson().fromJson(jsonString,foodList),null)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            Pair(null,ioException)
        }
    }
}
